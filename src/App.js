import React, { useState, useRef, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";

import setAuthToken from "./redux/AuthToken";
import store from "./redux/store";
import { loadUser } from "./redux/actions/AuthUser";

// layout
import DashboardNavbar from "./Layout/DashboardNavbar";
import DashboardSideBar from "./Layout/DashboardSideBar";
import PrivateRoute from "./Layout/PrivateRoute";

// Pages
import Dashboard from "./pages/Dashboard/Dashboard";
import LoginPage from "./pages/LoginPage/LoginPage";
import Chefs from "./pages/Chefs/Chefs";
import Users from "./pages/Users/Users";
import Bookings from "./pages/Bookings/Bookings";
import Payment from "./pages/Payments/Payment";

const NotFound = () => {
	return <p>Not found</p>;
};

// ----------------------------------------------------------------------

if (localStorage.fycChef) {
	setAuthToken(localStorage.fycChef);
}

function App() {
	useEffect(() => {
		store.dispatch(loadUser());
	}, []);
	const [sideBarOpen, setSideBarOpen] = useState(false);
	const wrapperRef = useRef(null);

	const openSidebar = () => {
		setSideBarOpen(!sideBarOpen);
	};

	const closeSidebar = () => {
		setSideBarOpen(!sideBarOpen);
	};
	return (
		<Router>
			<Switch>
				<Route exact path="/login" component={LoginPage} />
				<>
					<div className="grid-container">
						<DashboardNavbar
							sideBarOpen={sideBarOpen}
							openSidebar={openSidebar}
						/>
						<DashboardSideBar
							wrapperCont={wrapperRef}
							sideBarOpen={sideBarOpen}
							closeSidebar={closeSidebar}
						/>
						<div className="main">
							<PrivateRoute exact path="/" component={Dashboard} />
							<PrivateRoute exact path="/chefs" component={Chefs} />
							<PrivateRoute exact path="/users" component={Users} />
							<PrivateRoute exact path="/bookings" component={Bookings} />
							<PrivateRoute exact path="/payment" component={Payment} />
						</div>
					</div>
				</>
				<Route component={NotFound} />
			</Switch>
		</Router>
	);
}

export default App;
