import React, { useEffect } from "react";
import { connect } from "react-redux";
import { SiCodechef } from "react-icons/si";
import { ImUsers } from "react-icons/im";
import { GiFoodTruck } from "react-icons/gi";

import "./dashboard.css";
import { loadOverview } from "../../redux/actions/Overview";

const Dashboard = ({ loadOverview, overviews: { overview } }) => {
	useEffect(() => {
		loadOverview();
	}, [loadOverview]);
	return (
		<div className="dashboard">
			<h3>Dashboard Overview</h3>
			<div className="dashboard-container">
				<div className="container-item">
					<div className="container-icon chef">
						<SiCodechef />
					</div>
					<div className="container-figure">
						<h3>{overview && overview.totalChef}</h3>
						<p>Chefs</p>
					</div>
				</div>
				<div className="container-item">
					<div className="container-icon users">
						<ImUsers />
					</div>
					<div className="container-figure">
						<h3>{overview && overview.totalUsers}</h3>
						<p>Users</p>
					</div>
				</div>
				<div className="container-item">
					<div className="container-icon booking-color">
						<GiFoodTruck />
					</div>
					<div className="container-figure">
						<h3>{overview && overview.totalBookings}</h3>
						<p>Bookings</p>
					</div>
				</div>
			</div>
		</div>
	);
};

const mapStateToProps = (state) => ({
	overviews: state.overviewReducer,
});

export default connect(mapStateToProps, { loadOverview })(Dashboard);
