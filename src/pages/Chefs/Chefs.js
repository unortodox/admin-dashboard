import React, { useEffect, useState } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { ToastContainer } from "react-toastify";
import { Modal, Box, Typography } from "@mui/material";
import { connect } from "react-redux";
import { BiEditAlt } from "react-icons/bi";
import {
	loadChef,
	fetchChefDetail,
	updateChefDetail,
} from "../../redux/actions/Overview";
import "./chefs.css";
import "react-toastify/dist/ReactToastify.css";

const style = {
	position: "absolute",
	top: "50%",
	left: "50%",
	transform: "translate(-50%, -50%)",
	width: "50%",
	bgcolor: "background.paper",
	border: "2px solid #000",
	boxShadow: 24,
	p: 3,
};

const Chefs = ({
	loadChef,
	chefList,
	fetchChefDetail,
	chefDetail,
	loading,
	updateChefDetail,
}) => {
	const [details, setDetails] = useState({
		fullname: "",
		username: "",
		email: "",
		phone: "",
		id: "",
	});
	const [open, setOpen] = useState(false);
	const handleOpen = () => setOpen(true);
	const handleClose = () => setOpen(false);

	useEffect(() => {
		loadChef();
		setDetails({
			...details,
			username: !loading && !chefDetail?.username ? "" : chefDetail.username,
			fullname: !loading && !chefDetail?.fullname ? "" : chefDetail.fullname,
			email: !loading && !chefDetail?.email ? "" : chefDetail.email,
			phone: !loading && !chefDetail?.phoneNumber ? "" : chefDetail.phoneNumber,
			id: !loading && !chefDetail?._id ? "" : chefDetail._id,
		});
	}, [loadChef, chefDetail]);

	const handleClick = (event, cellValue) => {
		event.preventDefault();
		fetchChefDetail(cellValue.row._id);
		setTimeout(function () {
			handleOpen();
		}, 2000);
	};

	// Handle Form Change
	const handleChange = (e) => {
		setDetails({
			...details,
			[e.target.name]: e.target.value,
		});
	};

	// Handle Edit Submitted
	const { fullname, username, email, phone } = details;
	const body = JSON.stringify({
		fullname: fullname,
		username: username,
		email: email,
		phone: phone,
	});

	const handleEdit = (e) => {
		e.preventDefault();
		updateChefDetail(body, details.id);
		setTimeout(function () {
			setOpen(false);
		}, 5000);
	};
	console.log(chefList);
	return (
		<div className="chefs">
			<h3>Chefs Listing</h3>
			<div style={{ height: 400, width: "100%" }}>
				<DataGrid
					getRowId={(r) => r._id}
					rows={chefList}
					columns={[
						{
							field: "fullname",
							headerName: "FullName",
							width: 150,
						},
						{
							field: "username",
							headerName: "Username",
							width: 200,
						},
						{
							field: "email",
							headerName: "Email",
							width: 270,
						},
						{
							field: "phoneNumber",
							headerName: "Phone Number",
							width: 200,
						},
						{
							field: "categories",
							headerName: "Recipies",
							width: 270,
						},
						{
							field: "location",
							headerName: "Location",
							width: 270,
						},
						{
							field: "status",
							headerName: "Status",
							width: 170,
						},
						{
							field: "Edit",
							width: 170,
							renderCell: (cellValue) => {
								return (
									<button
										style={{
											padding: "5px 9px",
											backgroundColor: "#6191da",
											color: "#fff",
											border: "none",
											fontSize: "16px",
										}}
										onClick={(event) => {
											handleClick(event, cellValue);
										}}
									>
										<BiEditAlt />
									</button>
								);
							},
						},
					]}
					pageSize={5}
					rowsPerPageOptions={[5]}
					disableSelectionOnClick
				/>
			</div>
			<Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
			>
				<Box sx={style}>
					<Typography
						id="modal-modal-title"
						variant="h6"
						component="h6"
						style={{ color: "#775d09", fontWeight: "600" }}
					>
						{`Edit Chef ${chefDetail?.fullname} Record`}
					</Typography>

					<div>
						<form onSubmit={(e) => handleEdit(e)}>
							<div className="form-grouping">
								<label htmlFor="fullname">Full Name</label>
								<input
									type="text"
									value={details.fullname}
									name="fullname"
									onChange={(e) => handleChange(e)}
									placeholder="Full Name"
								/>
							</div>
							<div className="form-grouping">
								<label htmlFor="username">UserName</label>
								<input
									type="text"
									value={details.username}
									name="username"
									onChange={(e) => handleChange(e)}
									placeholder="userName"
								/>
							</div>
							<div className="form-grouping">
								<label htmlFor="username">Email Address</label>
								<input
									type="email"
									value={details.email}
									name="email"
									onChange={(e) => handleChange(e)}
									placeholder="Email Address"
								/>
							</div>
							<div className="form-grouping">
								<label htmlFor="username">Phone</label>
								<input
									type="number"
									value={details.phone}
									name="phone"
									onChange={(e) => handleChange(e)}
									placeholder="Phone"
								/>
							</div>
							<button className="btnSave" action="submit">
								Submit
							</button>
						</form>
					</div>
				</Box>
			</Modal>
			{/* Toast Alert */}
			<ToastContainer position="top-right" autoClose={10000} />
		</div>
	);
};

const mapStateToProps = (state) => ({
	chefList: state.overviewReducer.chefOverview,
	chefDetail: state.overviewReducer.chefDetail,
	loading: state.overviewReducer.loading,
});

export default connect(mapStateToProps, {
	loadChef,
	fetchChefDetail,
	updateChefDetail,
})(Chefs);
