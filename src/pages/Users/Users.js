import React, { useEffect, useState } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { Modal, Box, Typography } from "@mui/material";
import {
	loadUsers,
	fetchUserDetail,
	updateUserDetail,
	updateUserStatus,
} from "../../redux/actions/Overview";
import { BiEditAlt } from "react-icons/bi";
import "react-toastify/dist/ReactToastify.css";
import "./users.css";

const style = {
	position: "absolute",
	top: "50%",
	left: "50%",
	transform: "translate(-50%, -50%)",
	width: "50%",
	bgcolor: "background.paper",
	border: "2px solid #000",
	boxShadow: 24,
	p: 3,
};

const Users = ({
	loadUsers,
	userList,
	fetchUserDetail,
	userDetail,
	loading,
	updateUserDetail,
	updateUserStatus,
}) => {
	const [details, setDetails] = useState({
		fullname: "",
		username: "",
		email: "",
		phone: "",
		id: "",
	});
	const [open, setOpen] = useState(false);
	const handleOpen = () => setOpen(true);
	const handleClose = () => setOpen(false);

	useEffect(() => {
		loadUsers();
		setDetails({
			...details,
			username: !loading && !userDetail?.username ? "" : userDetail.username,
			fullname: !loading && !userDetail?.fullname ? "" : userDetail.fullname,
			email: !loading && !userDetail?.email ? "" : userDetail.email,
			phone: !loading && !userDetail?.phoneNumber ? "" : userDetail.phoneNumber,
			id: !loading && !userDetail?._id ? "" : userDetail._id,
		});
	}, [loadUsers, userDetail]);

	const handleClick = (event, cellValue) => {
		event.preventDefault();
		fetchUserDetail(cellValue.row._id);
		setTimeout(function () {
			handleOpen();
		}, 2000);
	};

	// Handle Form Change
	const handleChange = (e) => {
		setDetails({
			...details,
			[e.target.name]: e.target.value,
		});
	};

	// Handle Edit Submitted
	const { fullname, username, email, phone } = details;
	const body = JSON.stringify({
		fullname: fullname,
		username: username,
		email: email,
		phone: phone,
	});

	const handleEdit = (e) => {
		e.preventDefault();
		updateUserDetail(body, details.id);
		setTimeout(function () {
			setOpen(false);
		}, 5000);
	};

	return (
		<div className="user">
			<h3>Users Listing</h3>
			<div style={{ height: 400, width: "100%" }}>
				<DataGrid
					getRowId={(r) => r._id}
					rows={userList}
					columns={[
						{
							field: "fullname",
							headerName: "FullName",
							width: 150,
						},
						{
							field: "username",
							headerName: "Username",
							width: 160,
						},
						{
							field: "email",
							headerName: "Email Address",
							width: 270,
						},
						{
							field: "phoneNumber",
							headerName: "Phone Number",
							width: 270,
						},
						{
							field: "status",
							headerName: "Status",
							width: 170,
						},
						{
							field: "Edit",
							width: 150,
							renderCell: (cellValue) => {
								return (
									<>
										<button
											style={{
												padding: "5px 9px",
												backgroundColor: "#6191da",
												color: "#fff",
												border: "none",
												fontSize: "16px",
											}}
											onClick={(event) => {
												handleClick(event, cellValue);
											}}
										>
											<BiEditAlt />
										</button>
									</>
								);
							},
						},
						{
							field: "Actions",
							width: 200,
							style: {
								outline: "none",
							},
							renderCell: (cellValue) => {
								return (
									<>
										<button
											className={
												cellValue.row.status === "active"
													? "deActivate"
													: "activate"
											}
											style={{
												marginLeft: "10px",
												border: "none",
												padding: "6px 10px",
												fontSize: "14px",
												cusor: "pointer",
											}}
											onClick={(e) => {
												e.preventDefault();
												updateUserStatus(cellValue.row._id);
											}}
										>
											{cellValue.row.status === "active"
												? "Deactivate"
												: "Activate"}
										</button>
									</>
								);
							},
						},
					]}
					pageSize={10}
					rowsPerPageOptions={[10]}
					disableSelectionOnClick
				/>
			</div>
			<Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
			>
				<Box sx={style}>
					<Typography
						id="modal-modal-title"
						variant="h6"
						component="h6"
						style={{ color: "#775d09", fontWeight: "600" }}
					>
						{`Edit ${userDetail?.fullname} Record`}
					</Typography>

					<div>
						<form onSubmit={(e) => handleEdit(e)}>
							<div className="form-grouping">
								<label htmlFor="fullname">Full Name</label>
								<input
									type="text"
									value={details.fullname}
									name="fullname"
									onChange={(e) => handleChange(e)}
									placeholder="Full Name"
								/>
							</div>
							<div className="form-grouping">
								<label htmlFor="username">UserName</label>
								<input
									type="text"
									value={details.username}
									name="username"
									onChange={(e) => handleChange(e)}
									placeholder="userName"
								/>
							</div>
							<div className="form-grouping">
								<label htmlFor="username">Phone</label>
								<input
									type="text"
									value={details.phone}
									name="phone"
									onChange={(e) => handleChange(e)}
									placeholder="Phone"
								/>
							</div>
							<div className="form-grouping">
								<label htmlFor="username">Email Address</label>
								<input
									type="text"
									value={details.email}
									name="email"
									onChange={(e) => handleChange(e)}
									placeholder="Email Address"
								/>
							</div>
							<button className="btnSave" action="submit">
								Submit
							</button>
						</form>
					</div>
				</Box>
			</Modal>
			{/* Toast Alert */}
			<ToastContainer position="top-right" autoClose={10000} />
		</div>
	);
};

const mapStateToProps = (state) => ({
	userList: state.overviewReducer.userOverview,
	userDetail: state.overviewReducer.userDetail,
	loading: state.overviewReducer.loading,
});

export default connect(mapStateToProps, {
	loadUsers,
	fetchUserDetail,
	updateUserDetail,
	updateUserStatus,
})(withRouter(Users));
