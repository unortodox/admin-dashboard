import React, { useEffect, useState } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { connect } from "react-redux";
import { ToastContainer } from "react-toastify";
import { Modal, Box, Typography } from "@mui/material";

import "./bookings.css";
import { loadBooking, cancelOrder } from "../../redux/actions/Overview";
import "react-toastify/dist/ReactToastify.css";

const style = {
	position: "absolute",
	top: "50%",
	left: "50%",
	transform: "translate(-50%, -50%)",
	width: "45%",
	bgcolor: "background.paper",
	border: "2px solid #000",
	boxShadow: 24,
	p: 3,
};

const Bookings = ({ loadBooking, bookList, cancelOrder }) => {
	const [bookDesc, setBookDesc] = useState({
		description: "",
		guestNo: "",
		instructions: "",
	});
	useEffect(() => {
		loadBooking();
	}, [loadBooking]);

	console.log(bookDesc);

	const [open, setOpen] = useState(false);
	const handleOpen = () => setOpen(true);
	const handleClose = () => setOpen(false);

	const handleClick = (event, cellValue) => {
		event.preventDefault();
		console.log(cellValue.row.bookData);
		setBookDesc(
			{
				description: !cellValue.row.bookData
					? ""
					: cellValue.row.bookData.description,
				guestNo: !cellValue.row.bookData
					? ""
					: cellValue.row.bookData.noOfGuests,
				instructions: !cellValue.row.bookData
					? ""
					: cellValue.row.bookData.instructions,
			},
			[cellValue.bookData]
		);
		setTimeout(() => {
			handleOpen();
		}, 1000);
	};

	return (
		<div className="booking">
			<h3>Booking Listing</h3>

			<div style={{ height: 500, width: "100%" }}>
				<DataGrid
					getRowId={(r) => r._id}
					rows={bookList}
					columns={[
						{
							field: "bookersName",
							headerName: "Consumers Name",
							width: 250,
						},
						{
							field: "address",
							headerName: "Address",
							width: 370,
						},
						{
							field: "amount",
							headerName: "Amount($)",
							width: 160,
						},
						{
							field: "chefName",
							headerName: "Chef Name",
							width: 160,
						},
						{
							field: "serviceDate",
							headerName: "Service Date",
							width: 240,
						},
						{
							field: "Status",
							width: 200,

							renderCell: (cellValue) => {
								return (
									<>
										{/* || */}
										{cellValue.row.status === "approved" &&
										cellValue.row.payment === "success" ? (
											<button
												style={{
													marginLeft: "10px",
													border: "none",
													padding: "6px 10px",
													fontSize: "14px",
													cusor: "pointer",
													backgroundColor: "rgb(170, 57, 57)",
													color: "#fff",
												}}
												onClick={(e) => {
													e.preventDefault();
													cancelOrder(cellValue.row._id);
												}}
											>
												Cancel Order
											</button>
										) : (
											"Cancelled Booking"
										)}
									</>
								);
							},
						},
						{
							field: "bookData",
							headerName: "Booking Descriptions",
							width: 220,
							renderCell: (cellValue) => {
								return (
									<>
										<button
											style={{
												padding: "5px 8px",
												border: "none",
												backgroundColor: "#775d09",
												color: "#fff",
											}}
											onClick={(event) => handleClick(event, cellValue)}
										>
											View Descriptions
										</button>
									</>
								);
							},
						},
					]}
					pageSize={7}
					rowsPerPageOptions={[7]}
					disableSelectionOnClick
				/>
			</div>
			<Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="modal-modal-title"
				aria-describedby="modal-modal-description"
			>
				<Box sx={style}>
					<Typography
						id="modal-modal-title"
						variant="h6"
						component="h6"
						style={{ color: "#775d09", fontWeight: "600" }}
					>
						{`Booking Descriptions`}
					</Typography>

					<div style={{ fontSize: "16px" }}>
						<p>{`Number of Guest: ${bookDesc.guestNo}`}</p>
						<p>{`Booking Description: ${bookDesc.description}`}</p>
						<p>{`Cooking Instructions: ${bookDesc.instructions}`}</p>
					</div>
				</Box>
			</Modal>
			{/* Toast Alert */}
			<ToastContainer position="top-right" autoClose={10000} />
		</div>
	);
};

const mapStateToProps = (state) => ({
	bookList: state.overviewReducer.bookingOverview,
});

export default connect(mapStateToProps, { loadBooking, cancelOrder })(Bookings);
