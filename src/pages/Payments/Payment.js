import React, { useEffect } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { connect } from "react-redux";
import { ToastContainer } from "react-toastify";
import {
	paymentLog,
	processDecline,
	processPayment,
} from "../../redux/actions/Overview";
import "./payment.css";
import "react-toastify/dist/ReactToastify.css";

const Payment = ({ paymentLog, payments, processDecline, processPayment }) => {
	useEffect(() => {
		paymentLog();
	}, [paymentLog]);

	// Process Payment
	const processPaymentMode = (event, cellValue) => {
		event.preventDefault();
		processPayment(cellValue.row._id);
	};

	// decline decline StrictMode
	const processDeclineMode = (event, cellValue) => {
		event.preventDefault();
		processDecline(cellValue.row._id);
	};

	return (
		<div className="paymentContainer">
			<h3>The Payment Log</h3>

			<div style={{ height: 500, width: "100%" }}>
				<DataGrid
					getRowId={(r) => r._id}
					rows={payments}
					columns={[
						{ field: "chefName", headerName: "Chef Name", width: 170 },
						{
							field: "chefEmail",
							headerName: "Chef Email",
							width: 170,
						},
						{
							field: "chefPhone",
							headerName: "Chef Phone",
							width: 170,
						},
						{
							field: "bankName",
							headerName: "Bank Name",
							width: 170,
						},
						{
							field: "bankAccountNo",
							headerName: "Account Number",
							width: 190,
						},
						{
							field: "amount",
							headerName: "Amount($)",
							width: 220,
						},
						{
							field: "status",
							headerName: "Transaction Status",
							width: 220,
						},
						{
							field: "Transaction Actions",
							width: 270,
							renderCell: (cellValue) => {
								return (
									<>
										{cellValue.row.status === "PENDING" ? (
											<>
												<>
													<button
														style={{
															marginRight: "10px",
															padding: "6px",
															border: "none",
															backgroundColor: "#6191da",
															color: "#fff",
															outline: "none",
															cusor: "pointer",
														}}
														onClick={(event) =>
															processPaymentMode(event, cellValue)
														}
													>
														Process Payment
													</button>
												</>
												<>
													<button
														style={{
															marginRight: "10px",
															padding: "6px",
															border: "none",
															backgroundColor: "#5f2020",
															color: "#fff",
															outline: "none",
															cusor: "pointer",
														}}
														onClick={(event) =>
															processDeclineMode(event, cellValue)
														}
													>
														Decline Payment
													</button>
												</>
											</>
										) : cellValue.row.status === "DECLINED" ? (
											<p>Payment Declined</p>
										) : (
											<p>Payment Successful</p>
										)}
									</>
								);
							},
						},
					]}
					pageSize={7}
					rowsPerPageOptions={[7]}
					disableSelectionOnClick
				/>
			</div>
			{/* Toast Alert */}
			<ToastContainer position="top-right" autoClose={10000} />
		</div>
	);
};

const mapStateToProps = (state) => ({
	payments: state.overviewReducer.paymentOverview,
});

export default connect(mapStateToProps, {
	paymentLog,
	processPayment,
	processDecline,
})(Payment);
