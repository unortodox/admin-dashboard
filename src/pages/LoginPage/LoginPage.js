import React, { useState } from "react";
import { connect } from "react-redux";
import { withRouter, Redirect } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { loginUser } from "../../redux/actions/AuthUser";
import "react-toastify/dist/ReactToastify.css";
import "./loginpage.css";

const LoginPage = ({ loginUser, history, profile }) => {
	const [formData, setFormData] = useState({
		email: "",
		password: "",
	});

	// handle on Change event
	const handleChange = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value });
	};

	const { email, password } = formData;

	const loginCred = JSON.stringify({ email: email, password: password });

	const handleSubmit = (e) => {
		e.preventDefault();
		loginUser(loginCred, history);
	};

	if (profile.isAuthenticated === null) {
		<Redirect to="/" />;
	}

	return (
		<div className="login-container">
			<div className="login-page">
				<div className="login-body">
					<form onSubmit={(e) => handleSubmit(e)}>
						<div className="form_group">
							<label htmlFor="email">Email</label>
							<input
								type="email"
								placeholder="Email"
								name="email"
								onChange={(e) => handleChange(e)}
							/>
						</div>
						<div className="form_group">
							<label htmlFor="email">password</label>
							<input
								type="password"
								placeholder="password"
								name="password"
								onChange={(e) => handleChange(e)}
							/>
						</div>
						<button type="submit">Log In</button>
					</form>
				</div>
				{/* Toast Alert */}
				<ToastContainer position="top-right" autoClose={10000} />
			</div>
			<div className="login-design">
				<img src="https://www.findyourchef.com/images/logo.png" alt="Logo" />
				Find Your Chef
			</div>
		</div>
	);
};

const mapStateToProps = (state) => ({
	profile: state.Auth,
});

export default connect(mapStateToProps, { loginUser })(withRouter(LoginPage));
