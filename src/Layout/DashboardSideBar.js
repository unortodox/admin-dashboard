import React from "react";
import { NavLink } from "react-router-dom";
import "./DashboardSideBar.styles.css";
import { AiOutlineClose } from "react-icons/ai";
import { FiHome } from "react-icons/fi";
import { BiUser, BiFoodMenu } from "react-icons/bi";
import { MdPayment } from "react-icons/md";

const DashboardSidebar = ({ sideBarOpen, closeSidebar, wrapperCont }) => {
	return (
		<div
			ref={wrapperCont}
			className={sideBarOpen ? "sidebar-responsive sidebar" : "sidebar"}
		>
			<div onClick={() => closeSidebar()} className="sidebar__close-icon">
				<AiOutlineClose />
			</div>
			<div className={"sidebar__logo"}>
				<img src="https://www.findyourchef.com/images/logo.png" alt="Logo" />
			</div>
			<ul className="sidebar__navList">
				<li onClick={() => closeSidebar()}>
					<NavLink to={"/"}>
						<i>
							<FiHome />
						</i>
						<p className={"sidebar__navList--text"}>Home</p>
					</NavLink>
				</li>
				<li onClick={() => closeSidebar()}>
					<NavLink to={"/chefs"}>
						<i>
							<BiUser />
						</i>
						<p className={"sidebar__navList--text"}>All Chefs</p>
					</NavLink>
				</li>
				<li onClick={() => closeSidebar()}>
					<NavLink to={"/users"}>
						<i>
							<BiUser />
						</i>
						<p className={"sidebar__navList--text"}>All Users</p>
					</NavLink>
				</li>
				<li onClick={() => closeSidebar()}>
					<NavLink to={"/bookings"}>
						<i>
							<BiFoodMenu />
						</i>
						<p className={"sidebar__navList--text"}>All Bookings</p>
					</NavLink>
				</li>
				<li onClick={() => closeSidebar()}>
					<NavLink to={"/payment"}>
						<i>
							<MdPayment />
						</i>
						<p className={"sidebar__navList--text"}>Chef Payment</p>
					</NavLink>
				</li>
			</ul>
		</div>
	);
};

export default DashboardSidebar;
