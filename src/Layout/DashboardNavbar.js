import React from "react";
import "./DashboardNavbar.styles.css";
import { connect } from "react-redux";
import { BiMenu } from "react-icons/bi";
import { logout } from "../redux/actions/AuthUser";

const DashboardNavbar = ({ openSidebar, auth: { user }, logout }) => {
	return (
		<header className="header">
			<div onClick={() => openSidebar()} className="header__icon">
				<BiMenu />
			</div>
			<div className="header__content">
				{/* User */}
				<div className="user-name">
					<h3 style={{ fontWeight: "700" }}>{user && user.username}</h3>
				</div>
				<div onClick={() => logout()} className="user-logout">
					<h3>logout</h3>
				</div>
			</div>
		</header>
	);
};

const mapStateToProps = (state) => ({
	auth: state.Auth,
});

export default connect(mapStateToProps, { logout })(DashboardNavbar);
