const AuthTypes = {
	LOGIN_USER: "LOGIN_USER",
	LOAD_USER: "LOAD_USER",
	USER_CLEAR: "USER_CLEAR",
};

export default AuthTypes;
