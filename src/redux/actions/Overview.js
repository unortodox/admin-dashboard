import axios from "axios";
import { toast } from "react-toastify";
import OverviewTypes from "../types/overview.types";
const BASE_URL = "https://thepotters-api.herokuapp.com/api/v1/";

// load overview
export const loadOverview = () => async (dispatch) => {
	try {
		const overview = await axios.get(`${BASE_URL}admin/overview`);
		dispatch({
			type: OverviewTypes.OVERVIEW_LOAD,
			payload: overview.data.payload.data,
		});
	} catch (err) {
		console.warn(err.message);
	}
};

// load chef list
export const loadChef = () => async (dispatch) => {
	try {
		const chefList = await axios.get(`${BASE_URL}admin/chefs`);
		dispatch({
			type: OverviewTypes.CHEF_OVERVIEW,
			payload: chefList.data.payload.data.data,
		});
	} catch (err) {
		console.warn(err.message);
	}
};

// Fetch Individual Chef Record
export const fetchChefDetail = (id) => async (dispatch) => {
	try {
		const chefDetails = await axios.get(`${BASE_URL}admin/chefs/profile/${id}`);

		dispatch({
			type: OverviewTypes.CHEF_DETAIL,
			payload: chefDetails.data.payload.data,
		});
	} catch (err) {
		console.warn(err.message);
	}
};

// Update Chef record
export const updateChefDetail = (body, id) => async (dispatch) => {
	const config = {
		headers: { "Content-Type": "application/json" },
	};
	try {
		const updateRecord = await axios.put(
			`${BASE_URL}admin/chefs/profile/${id}`,
			body,
			config
		);
		if (updateRecord.status === 200) {
			toast.success("Record Update Successfull");
		}
		dispatch(loadChef());
	} catch (err) {
		if (err.response && err.response.data) {
			toast.error(err.response.data.error.message);
		}
	}
};

// load users list
export const loadUsers = () => async (dispatch) => {
	try {
		const userList = await axios.get(`${BASE_URL}admin/users`);

		dispatch({
			type: OverviewTypes.USER_OVERVIEW,
			payload: userList.data.payload.data.data,
		});
	} catch (err) {
		console.warn(err.message);
	}
};

// Fetch Individual User Record
export const fetchUserDetail = (id) => async (dispatch) => {
	dispatch({ type: OverviewTypes.CLEAR_USER_DETAIL });
	try {
		const UserDetails = await axios.get(`${BASE_URL}admin/users/profile/${id}`);
		dispatch({
			type: OverviewTypes.USER_DETAIL,
			payload: UserDetails.data.payload.data,
		});
	} catch (err) {
		console.warn(err.message);
	}
};

// Change User Status
export const updateUserStatus = (id) => async (dispatch) => {
	try {
		const UserDetailsStatus = await axios.put(
			`${BASE_URL}admin/users/status/${id}`
		);
		dispatch(loadUsers());
	} catch (err) {
		console.warn(err.message);
	}
};

// Update User record
export const updateUserDetail = (body, id) => async (dispatch) => {
	const config = {
		headers: { "Content-Type": "application/json" },
	};
	try {
		const updateRecord = await axios.put(
			`${BASE_URL}admin/users/profile/${id}`,
			body,
			config
		);
		if (updateRecord.status === 200) {
			toast.success("Record Update Successfull");
		}
		dispatch(loadUsers());
	} catch (err) {
		if (err.response && err.response.data) {
			toast.error(err.response.data.error.message);
		}
	}
};

// load Booking list
export const loadBooking = () => async (dispatch) => {
	try {
		const bookingList = await axios.get(`${BASE_URL}admin/bookings`);
		dispatch({
			type: OverviewTypes.BOOKING_OVERVIEW,
			payload: bookingList.data.payload.data.data,
		});
	} catch (err) {
		console.warn(err.message);
	}
};

// Cancel Client Order
export const cancelOrder = (id) => async (dispatch) => {
	try {
		const clientOrder = await axios.post(
			`${BASE_URL}admin/bookings/cancel/${id}`
		);
		console.log(clientOrder);

		if (clientOrder.status === 200) {
			toast.success(clientOrder.data.payload.data);
		}
		dispatch(loadBooking());
	} catch (err) {
		console.warn(err.message);
	}
};

// Get Payment Request List
export const paymentLog = () => async (dispatch) => {
	try {
		const paymentListing = await axios.get(`${BASE_URL}admin/payment/request`);
		dispatch({
			type: OverviewTypes.PAYMENT_OVERVIEW,
			payload: paymentListing.data.payload.data.data,
		});
	} catch (err) {
		console.warn(err.message);
	}
};

// Process payment

export const processPayment = (id) => async (dispatch) => {
	try {
		const paymentProcessing = await axios.put(
			`${BASE_URL}admin/payment/request/completed/${id}`
		);
		console.log(paymentProcessing);
		if (paymentProcessing.status === 200) {
			toast.success(paymentProcessing.data.payload.data);
		}
		dispatch(paymentLog());
	} catch (err) {
		console.warn(err.message);
	}
};

// Process Decline
export const processDecline = (id) => async (dispatch) => {
	try {
		const paymentDecline = await axios.put(
			`${BASE_URL}admin/payment/request/declined/${id}`
		);
		console.log(paymentDecline);
		if (paymentDecline.status === 200) {
			toast.success(paymentDecline.data.payload.data);
		}
		dispatch(paymentLog());
	} catch (err) {
		console.warn(err.message);
	}
};
