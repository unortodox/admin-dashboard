import axios from "axios";
import AuthTypes from "../types/auth.types";
import setAuthToken from "../AuthToken";
import { toast } from "react-toastify";
const BASE_URL = "https://thepotters-api.herokuapp.com/api/v1/";

// Load user

export const loadUser = () => async (dispatch) => {
	try {
		dispatch({ type: AuthTypes.LOAD_USER });
		if (localStorage.fycChef) {
			setAuthToken(localStorage.fycChef);
		}
	} catch (err) {
		console.warn(err.message);
	}
};

// Login users
export const loginUser = (loginCred, history) => async (dispatch) => {
	const config = {
		headers: { "Content-Type": "application/json" },
	};

	try {
		const loginMain = await axios.post(`${BASE_URL}login`, loginCred, config);
		dispatch({
			type: AuthTypes.LOGIN_USER,
			payload: loginMain.data.payload,
		});
		dispatch(loadUser());
		history.push("/");
	} catch (err) {
		if (err.response && err.response.data) {
			toast.error(err.response.data.error.message);
		}
	}
};

// Logout
export const logout = () => (dispatch) => {
	dispatch({
		type: AuthTypes.USER_CLEAR,
	});
};
