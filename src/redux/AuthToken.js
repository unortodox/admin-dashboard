import axios from "axios";

const setAuthToken = (fycChef) => {
	if (fycChef) {
		axios.defaults.headers.common["Authorization"] = `Bearer ${fycChef}`;
	} else {
		delete axios.defaults.headers.common["Authorization"];
	}
};

export default setAuthToken;
