import { combineReducers } from "redux";
import Auth from "./Auth";
import overviewReducer from "./overviewReducer";

export default combineReducers({
	Auth,
	overviewReducer,
});
