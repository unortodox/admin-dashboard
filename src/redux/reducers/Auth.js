import AuthTypes from "../types/auth.types";

const initialState = {
	token: localStorage.getItem("fycChef"),
	isAuthenticated: null,
	user: null,
	loading: true,
};

export default function (state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case AuthTypes.LOAD_USER:
			return {
				...state,
				isAuthenticated: localStorage.fycChef ? "true" : null,
				user: JSON.parse(localStorage.getItem("userDetail")),
				loading: false,
			};
		case AuthTypes.LOGIN_USER:
			localStorage.setItem("fycChef", payload.token);
			localStorage.setItem("userDetail", JSON.stringify(payload.data));
			return {
				...state,
				...payload,
				isAuthenticated: true,
				loading: false,
			};
		case AuthTypes.USER_CLEAR:
			localStorage.removeItem("fycChef");
			localStorage.removeItem("userDetail");
			return {
				...state,
				token: null,
				loading: false,
				isAuthenticated: false,
			};
		default:
			return state;
	}
}
