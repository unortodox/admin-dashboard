import OverviewTypes from "../types/overview.types";

const initialState = {
	overview: null,
	chefOverview: [],
	chefDetail: null,
	userDetail: null,
	userOverview: [],
	bookingOverview: [],
	paymentOverview: [],
	loading: true,
};

export default function (state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case OverviewTypes.OVERVIEW_LOAD:
			return {
				...state,
				overview: payload,
				loading: false,
			};

		case OverviewTypes.CHEF_OVERVIEW:
			return {
				...state,
				chefOverview: payload,
				loading: false,
			};
		case OverviewTypes.CHEF_DETAIL:
			return {
				...state,
				chefDetail: payload,
				loading: false,
			};
		case OverviewTypes.USER_OVERVIEW:
			return {
				...state,
				userOverview: payload,
				loading: false,
			};
		case OverviewTypes.USER_DETAIL:
			return {
				...state,
				userDetail: payload,
				loading: false,
			};
		case OverviewTypes.BOOKING_OVERVIEW:
			return {
				...state,
				bookingOverview: payload,
				loading: false,
			};
		case OverviewTypes.PAYMENT_OVERVIEW:
			return {
				...state,
				paymentOverview: payload,
				loading: false,
			};
		case OverviewTypes.CLEAR_CHEF_DETAIL:
			return {
				...state,
				chefDetail: null,
				loading: false,
			};
		case OverviewTypes.CLEAR_USER_DETAIL:
			return {
				...state,
				userDetail: null,
				loading: false,
			};
		case OverviewTypes.CLEAR_USER_OVERVIEW:
			return {
				...state,
				userOverview: [],
				loading: false,
			};
		default:
			return state;
	}
}
